// Ajax
let target_url = "https://jsonplaceholder.typicode.com/users";

function getData(url, callback) {
    let xhr = new XMLHttpRequest();
    // HTML Event pada Javascript: onload event
    xhr.onload = function() {
        if (xhr.status == 200) {
            let result = xhr.responseText;
            return callback(JSON.parse(result));
        }
    };
    // XML Ajax request: method open() dan send()
    xhr.open("GET", url);
    xhr.send();
}

let list = document.getElementById("list-data"); // Document Object Model: mengambil value melalui id
// callback
getData(target_url, (data) => {
    result = '';
    data.forEach((element) => {
        
        result += `
        <tr>
            <th scope="row">${element.id}</th>
            <td>${element.name}</td>
            <td>${element.username}</td>
            <td>${element.email}</td>
            <td>${element.address.street} ${element.address.suite} ${element.address.city} </td>
            <td>${element.company.name}</td>
          </tr>
        `;
    });
    // menampilkan output menggunakan property inner.HTML
    list.innerHTML = result;
});